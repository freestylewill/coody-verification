package org.coody.verification.test;

import org.coody.verification.annotation.ParamCheck;
import org.coody.verification.constant.FormatConstant;
import org.coody.verification.model.BaseModel;

@SuppressWarnings("serial")
public class UserInfo extends BaseModel{

	@ParamCheck(format=FormatConstant.USER_NAME)
	private String userName;
	@ParamCheck(format=FormatConstant.USER_PWD)
	private String password;
	@ParamCheck(format=FormatConstant.MOBILE, allowNull=true,orNulls="email")
	private String mobile;
	@ParamCheck(format=FormatConstant.EMAIL, allowNull=true,orNulls="mobile")
	private String email;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
