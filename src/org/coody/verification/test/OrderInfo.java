package org.coody.verification.test;

import java.util.Date;

import org.coody.verification.annotation.ParamCheck;
import org.coody.verification.constant.FormatConstant;
import org.coody.verification.model.BaseModel;

@SuppressWarnings("serial")
public class OrderInfo extends BaseModel{

	@ParamCheck
	private UserInfo userInfo;
	
	@ParamCheck(format=FormatConstant.POSITIVE_NUMBER)
	private String orderId;
	
	@ParamCheck
	private Date date;

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
